<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;


/**
 * DemoController implements the CRUD actions for Demo model.
 */
class UserController extends Controller
{
    public function actionIndex()
    {
        return $this->goHome();	
    }

    public function actionAdd()
    {
		$user = new User();
		$user->username = "mor";
		$user->password = "1";
		$user->save();
		$user = new User();
		$user->username = "itay";
		$user->password = "2";
		$user->save();	
		$user = new User();
		$user->username = "aviv";
		$user->password = "3";
		$user->save();
		$user = new User();
		$user->username = "daniel";
		$user->password = "4";
		$user->save();
		$user = new User();
		$user->username = "ben";
		$user->password = "5";
		$user->save();
		return $this->goHome();		
    }
}