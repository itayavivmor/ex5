<?php

namespace app\models;
use \yii\db\ActiveRecord;

class Student extends ActiveRecord
{
	const 	STATUS_INACTIVE = 0;
	const 	STATUS_ACTIVE = 1;

    private static function tableName()
	{
		return 'student';
	}

	public function getName()
	{
		return $this->name;
	}

}